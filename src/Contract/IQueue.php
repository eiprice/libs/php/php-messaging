<?php


namespace Eiprice\Messaging\Contract;

/**
 * Interface IQueue
 * @package Eiprice\Messaging\Contract
 */
interface IQueue
{
    /**
     * IQueue constructor.
     * @param $params
     */
    public function __construct($params);

    /**
     * @param $queue_name
     * @return mixed
     */
    public function set_queue_name($queue_name) : void;

    /**
     * @param callable $callback
     * @return mixed
     */
    public function set_callback(callable $callback) : void;

    /**
     * @return mixed
     */
    public function listen() : void;

    /**
     * @return mixed
     */
    public function forever() : void;
}
