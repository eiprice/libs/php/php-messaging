<?php


namespace Eiprice\Messaging\Contract;


interface IQueueMessage
{
    /**
     * @param bool $requeue Send back to queue
     *
     * @return mixed
     */
    public function nack($requeue = false);

    /**
     * @param bool $requeue Send back to queue
     *
     * @return mixed
     */
    public function reject($requeue = false);

    /**
     * @return mixed
     */
    public function ack();

    /**
     * @return mixed
     */
    public function getMessage();
}
