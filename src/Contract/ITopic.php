<?php


namespace Eiprice\Messaging\Contract;

/**
 * Interface Topic
 * @package Eiprice\Messaging\Contract
 */
interface ITopic
{
    function __construct($params);
    public function publish($message);
    public function setTopicName($topic_name);
}
