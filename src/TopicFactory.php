<?php

namespace Eiprice\Messaging;

use Eiprice\Messaging\Contract\ITopic;
use Eiprice\Messaging\Drivers\AWS\Sns;
use Eiprice\Messaging\Drivers\GCP\Pubsub;
use Eiprice\Messaging\Drivers\RabbitMQ\Topic as RabbitMQTopic;

/**
 * Class TopicFactory
 * @package Eiprice\Messaging
 */
class TopicFactory
{
    protected static $topic_list = [
        'AWS' => Sns::class,
        'GCP' => Pubsub::class,
        'RabbitMQ' => RabbitMQTopic::class
    ];

    /**
     * @param mixed ...$args
     * @return ITopic
     */
    public static function start_topic(...$args) : ITopic
    {
        $vendor_name = array_shift($args);

        if ( function_exists('app')) {
            return app()->make(self::$topic_list[$vendor_name], ["params" => $args]);
        } else{
            return new $vendor_name(...$args);
        }
    }
}
