<?php

namespace Eiprice\Messaging;

use Eiprice\Messaging\Contract\IQueue;
use Eiprice\Messaging\Drivers\AWS\Sqs;
use Eiprice\Messaging\Drivers\GCP\Pubsub;
use Eiprice\Messaging\Drivers\RabbitMQ\Queue as RabbitMQQueue;

/**
 * Class QueueFactory
 * @package Eiprice\Messaging
 */
class QueueFactory
{
    protected static $queue_list = [
        'AWS' => Sqs::class,
        'GCP' => Pubsub::class,
        'RabbitMQ' => RabbitMQQueue::class
    ];

    /**
     * @param mixed ...$args
     * @return IQueue
     */
    public static function start_queueu(...$args) : IQueue
    {
        $vendor_name = array_shift($args);

        if ( function_exists('app')) {
            return app()->make(self::$queue_list[$vendor_name], ["params" => $args]);
        } else{
            return new $vendor_name(...$args);
        }
    }
}
