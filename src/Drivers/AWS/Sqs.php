<?php


namespace Eiprice\Messaging\Drivers\AWS;

use Aws\Credentials\Credentials;
use Aws\Sqs\SqsClient;
use Eiprice\Messaging\Contract\IQueue;

/**
 * Class Sqs
 * @package Eiprice\Messaging\Drivers\AWS
 */
class Sqs implements IQueue
{
    /**
     * @var callable
     */
    protected $callback;

    /**
     * @var string
     */
    protected $queue_name;

    /**
     * @var string
     */
    protected $queue_url;

    /**
     * @var SqsClient
     */
    protected $AmazonSQS;

    /**
     * @var int $WaitTimeSeconds
     */
    protected $WaitTimeSeconds = 3;

    /**
     * @var int $MaxNumberOfMessages
     */
    protected $MaxNumberOfMessages = 10;


    public function __construct($params)
    {
        $credentials = new Credentials($params[0]['access_key'], $params[0]['secret_key']);

        $this->AmazonSQS = new SqsClient(
            [
                'region' => $params[0]['region'],
                'version' => 'latest',
                'credentials' => $credentials
            ]
        );

        $this->queue_url = $params[0]['queue_url'];

        if (isset($params[0]['WaitTimeSeconds'])){
            $this->WaitTimeSeconds = $params[0]['WaitTimeSeconds'];
        }

        if (isset($params[0]['MaxNumberOfMessages'])){
            $this->WaitTimeSeconds = $params[0]['MaxNumberOfMessages'];
        }
    }

    public function set_queue_name($queue_name)
    {
        $this->queue_name = $queue_name;
    }

    public function set_callback(callable $callback)
    {
        $this->callback = $callback;
    }

    public function listen(): void
    {
        $res = $this->AmazonSQS->receiveMessage(array(
            'QueueUrl'          => $this->queue_url,
            'WaitTimeSeconds'   => $this->WaitTimeSeconds,
            'MaxNumberOfMessages' => 20,
        ));

        if ($res->get('Messages')) {

            foreach ($res->get('Messages') as $msg) {

                $this->callback(
                    new SqsMessage(
                        $this->AmazonSQS,
                        $msg,
                        $this->queue_url
                    )
                );

            }
        }
    }

    /**
     *
     */
    public function forever(): void
    {
        while(true) {
            $this->listen();
        }
    }


}
