<?php


namespace Eiprice\Messaging\Drivers\AWS;


use Eiprice\Messaging\Contract\IQueueMessage;
use Aws\Sqs\SqsClient;

class SqsMessage implements IQueueMessage
{
    /**
     * @var SqsClient
     */
    protected $client;

    /**
     * @var String
     */
    protected $queue_url;


    /**
     * SqsMessage constructor.
     * @param SqsClient $client
     * @param $msg
     * @param $queue_url
     */
    public function __construct(SqsClient $client, $msg, $queue_url)
    {
        $this->client = $client;
        $this->result = $msg;
        $this->queue_url = $queue_url;
    }


    /**
     * @param bool $requeue
     * @return mixed|void
     */
    public function nack($requeue = false)
    {
        // DO Nothing
    }

    /**
     * @return mixed|void
     */
    public function ack()
    {
        $this->client->deleteMessageBatch(
            array(
                'QueueUrl' => $this->queue_url,
                'Entries' => array(
                    array(
                        'Id' => $this->getMessageId(),
                        'ReceiptHandle' => $this->getReceiptHandle(),
                    )
                )
            )
        );
    }

    /**
     * @return mixed
     */
    protected function getMessageId()
    {
        return $this->result['MessageId'];
    }

    /**
     * @return mixed
     */
    protected function getReceiptHandle()
    {
        return $this->result['ReceiptHandle'];
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->result['Body'];
    }

}
