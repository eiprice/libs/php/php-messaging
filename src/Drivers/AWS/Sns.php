<?php


namespace Eiprice\Messaging\Drivers\AWS;

use Aws\Credentials\Credentials;
use Eiprice\Messaging\Contract\ITopic;
use Aws\Sns\SnsClient;

/**
 * Class Sns
 * @package Eiprice\Messaging\Drivers\AWS
 */
class Sns implements ITopic
{
    /**
     * @var SnsClient
     */
    protected $AmazonSNS;
    protected $topicName;

    function __construct($params)
    {
        $credentials = new Credentials($params[0]['access_key'], $params[0]['secret_key']);

        $this->AmazonSNS = new SnsClient(
            [
                'region' => $params[0]['region'],
                'version' => 'latest',
                'credentials' => $credentials
            ]
        );

    }

    public function setTopicName($topic_name)
    {
        $result = $this->AmazonSNS->listTopics([])->toArray();

        foreach ($result['Topics'] as $topic){
            if ( strpos($topic['TopicArn'], $topic_name)){
                $this->topicName = $topic['TopicArn'];
            }
        }

        return $this;
    }

    /**
     *
     * @param $message
     * @param array $attributes
     */
    public function publish($message, $attributes = [])
    {
        /*foreach ($attributes as $label => $attribute){
            // Set the Topic's Display Name (required)
            $this->AmazonSNS->setTopicAttributes($topicArn, $label, $attribute);
        }*/

        // And send a message to subscribers of this topic
        $this->AmazonSNS->publish([
            'Message' => $message,
            'TopicArn' => $this->topicName,
        ]);
    }
}
