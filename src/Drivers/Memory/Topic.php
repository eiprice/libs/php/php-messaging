<?php


namespace Eiprice\Messaging\Drivers\Memory;


use Eiprice\Messaging\Contract\ITopic;

class Topic implements ITopic
{
    public $messages = [];
    public $topic = '';

    public function __construct($params = [])
    {

    }

    public function publish($message)
    {
        $this->messages[$this->topic][] = $message;
    }

    public function setTopicName($topic_name)
    {
        $this->topic = $topic_name;
        if ( !isset($this->messages[$this->topic])){
            $this->messages[$this->topic] = [];
        }
    }

}
