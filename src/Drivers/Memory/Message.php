<?php

namespace Eiprice\Messaging\Drivers\Memory;


use Eiprice\Messaging\Contract\IQueueMessage;

class Message implements IQueueMessage
{
    protected $message;

    protected $ack;

    function __construct($message = '')
    {
        $this->message = $message;
    }

    public function nack($requeue = false)
    {
        $this->ack = false;
    }

    public function reject($requeue = false)
    {
        // TODO: Implement reject() method.
    }

    public function ack()
    {
        $this->ack = true;
    }

    public function getMessage()
    {
        return $this->message;
    }

}
