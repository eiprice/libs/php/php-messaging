<?php


namespace Eiprice\Messaging\Drivers\RabbitMQ;

use Eiprice\Messaging\Contract\IQueueMessage;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

/**
 * Class Message
 * @package Eiprice\Messaging\Drivers\RabbitMQ
 */
class Message
    implements IQueueMessage
{
    /**
     * @var AMQPMessage $msg
     */
    protected $msg;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Message constructor.
     * @param AMQPMessage $msg
     */
    function __construct(AMQPMessage $msg)
    {
        $this->msg = $msg;
        $this->logger = app()->make(LoggerInterface::class);
        $this->logger->debug("New message from Queue", (array) $msg);
    }


    public function getMessage()
    {
        return $this->msg->body;
    }

    /**
     * @var AMQPChannel $channel
     */
    protected function getChannel() : AMQPChannel
    {
        return $this->msg->delivery_info['channel'];
    }

    /**
     * @return mixed
     */
    protected function getDeliveryTag()
    {
        return $this->msg->delivery_info['delivery_tag'];
    }


    /**
     *
     * @return mixed|void
     */
    public function nack($requeue = false)
    {
        $this->getChannel()
            ->basic_nack(
                $this->getDeliveryTag(),
                false,
                $requeue
            )
        ;
    }

    /**
     *
     * @return mixed|void
     */
    public function reject($requeue = false)
    {
        $this->getChannel()
            ->basic_reject(
                $this->getDeliveryTag(),
                $requeue
            )
        ;
    }

    /**
     *
     * @return mixed|void
     */
    public function ack()
    {
        $this->getChannel()
            ->basic_ack($this->getDeliveryTag());
    }

}
