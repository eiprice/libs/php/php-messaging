<?php

namespace Eiprice\Messaging\Drivers\RabbitMQ;

use Eiprice\Messaging\Contract\IQueue;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use PhpAmqpLib\Channel\AMQPChannel;
use Psr\Log\LoggerInterface;

/**
 * Class Queue
 * @package Eiprice\Messaging\Drivers\RabbitMQ
 */
class Queue implements IQueue
{
    /**
     * @var callable
     */
    protected $callback;

    protected $queue_host;
    protected $queue_port;
    protected $queue_user;
    protected $queue_pass;
    protected $queue_name;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    protected $id;

    /**
     * @var LoggerInterface $logger
     */
    protected $logger;

    protected $params = [];

    /**
     * Queue constructor.
     * @param $params
     */
    public function __construct($params)
    {
        $this->queue_host = $params[0]['host'];
        $this->queue_port = $params[0]['port'];
        $this->queue_user = $params[0]['user'];
        $this->queue_pass = $params[0]['pass'];

        $this->logger = app()->make(LoggerInterface::class);

        $this->id = uniqid("queue");

        if ( isset($params[0]['params'])){
            $this->params = $params[0]['params'];
        }
    }


    protected function getParams()
    {
        return $this->params;
    }


    /**
     * @param $queue_name
     */
    public function set_queue_name($queue_name) : void
    {
        $this->queue_name = $queue_name;
    }

    /**
     * @param callable $callback
     */
    public function set_callback(callable $callback) : void
    {
        $this->callback = function (AMQPMessage $msg) use ($callback) {
            $callback(new Message($msg));
        };
    }

    /**
     *
     */
    protected function setup_listen() : void
    {
        $connection = new AMQPStreamConnection(
            $this->queue_host,
            $this->queue_port,
            $this->queue_user,
            $this->queue_pass
        );
        $this->channel = $connection->channel();

        $this->channel->queue_declare(
            $this->queue_name,
            false,
            true,
            false,
            false,
            false,
            new AMQPTable(
                $this->getParams()
            )
        );

        $this->channel->basic_consume(
            $this->queue_name,
            '',
            false,
            false,
            false,
            false,
            $this->callback
        );

        if ( isset($this->params['x-dead-letter-routing-key'])) {
            $this->channel->queue_bind(
                $this->queue_name,
                $this->params['x-dead-letter-exchange'],
                $this->params['x-dead-letter-routing-key']
            );
        }
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'queue' => [
                'name' => $this->queue_name,
                'host' => $this->queue_host,
                'user' => $this->queue_user,
            ],
        ];
    }

    /**
     *
     */
    public function listen() : void
    {
        $this->logger->info("Listening on queue {$this->queue_name}", $this->toArray());


        $this->setup_listen();

        while ($n = count($this->channel->callbacks)) {
            $this->channel->wait();
        }
    }

    /**
     *
     */
    public function forever() : void
    {
        while (true){
            try{
                $this->listen();
            }catch (\Throwable $e){
                echo $e->getMessage() . PHP_EOL;
                echo $e->getFile() . PHP_EOL;
                echo $e->getLine() . PHP_EOL;
            }

            $this->logger->info("Restarting listening: {$this->id}");
            sleep(3);
        }
    }
}
