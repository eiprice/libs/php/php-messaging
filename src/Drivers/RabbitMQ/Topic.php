<?php


namespace Eiprice\Messaging\Drivers\RabbitMQ;


use Eiprice\Messaging\Contract\ITopic;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use PhpAmqpLib\Channel\AMQPChannel;
use phpDocumentor\Reflection\Types\Null_;
use Psr\Log\LoggerInterface;

/**
 * Class Queue
 * @package Eiprice\Messaging\Drivers\RabbitMQ
 */
class Topic
    implements ITopic
{
    protected $topic_host;
    protected $topic_port;
    protected $topic_user;
    protected $topic_pass;
    protected $topic_name;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    protected $id;

    /**
     * @var LoggerInterface $logger
     */
    protected $logger;

    protected $params = [];

    /**
     * Queue constructor.
     * @param $params
     */
    public function __construct($params)
    {
        $this->topic_host = $params[0]['host'];
        $this->topic_port = $params[0]['port'];
        $this->topic_user = $params[0]['user'];
        $this->topic_pass = $params[0]['pass'];

        $this->logger = app()->make(LoggerInterface::class);

        $this->id = uniqid("topic");

        if ( isset($params[0]['params'])){
            $this->params = $params[0]['params'];
        }
    }


    protected function getParams()
    {
        return $this->params;
    }


    /**
     * @param $topic_name
     */
    public function setTopicName($topic_name) : void
    {
        $this->topic_name = $topic_name;
    }

    /**
     * @param callable $callback
     */
    public function set_callback(callable $callback) : void
    {
        $this->callback = function (AMQPMessage $msg) use ($callback) {
            $callback(new Message($msg));
        };
    }

    /**
     *
     */
    protected function setup() : void
    {
        $connection = new AMQPStreamConnection(
            $this->topic_host,
            $this->topic_port,
            $this->topic_user,
            $this->topic_pass
        );

        $this->channel = $connection->channel();

    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'topic' => [
                'name' => $this->topic_name,
                'host' => $this->topic_host,
                'user' => $this->topic_user,
            ],
        ];
    }

    /**
     * @return AMQPChannel|null
     */
    protected function getChannel() : AMQPChannel
    {
        if ( $this->channel === null){
            $this->setup();
        }
        return $this->channel;
    }

    public function publish($message)
    {
        $this->logger->debug("Sending message: {$message}", ['topic' => $this->topic_name]);
        $msg = new AMQPMessage($message);

        $this->getChannel()->basic_publish(
            $msg,
            $this->topic_name,
            ''
        );
    }
}
