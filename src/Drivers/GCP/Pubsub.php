<?php

namespace Eiprice\Messaging\Drivers\GCP;

use Google\Cloud\PubSub\PubSubClient;
use Eiprice\Messaging\Contract\ITopic;
use Google\Cloud\PubSub\Topic;


/**
 * Class GoogleTopic
 * @package Eiprice\Messaging\Drivers\GCP
 */
class Pubsub implements ITopic
{
    /**
     * @var PubSubClient
     */
    protected $pubsub;
    /**
     * @var Topic[]
     */
    protected $topic = [];
    protected $topic_name = '';


    public function __construct($params)
    {
        $this->pubsub = new PubSubClient(['projectId' => $params[0]['projectId']]);
    }

    /**
     * @param $message
     */
    public function publish($message)
    {
        $this->topic[$this->topic_name]->publish(['data' => $message]);
    }

    public function setTopicName($topic_name)
    {
        $this->topic_name = $topic_name;
        $this->topic[$topic_name] = $this->pubsub->topic($topic_name);
    }

}
