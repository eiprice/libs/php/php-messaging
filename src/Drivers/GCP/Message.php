<?php


namespace Eiprice\Messaging\Drivers\GCP;


use Eiprice\Messaging\Contract\IQueueMessage;

class Message
    implements IQueueMessage
{
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function nack($requeue = false)
    {
        // TODO: Implement nack() method.
    }

    public function reject($requeue = false)
    {
        // TODO: Implement reject() method.
    }

    public function ack()
    {
        // TODO: Implement ack() method.
    }

    public function getMessage()
    {
        return $this->data;
    }

}
