<?php


namespace Eiprice\Messaging\Drivers\GCP;


use Eiprice\Messaging\Contract\IQueue;

class QueueWebhook implements IQueue
{
    /**
     * @var callable
     */
    protected $callback;

    public function set_queue_name($queue_name): void
    {
        // TODO: Implement set_queue_name() method.
    }

    /**
     * @param callable $callback
     */
    public function set_callback(callable $callback) : void
    {
        $this->callback = function ($msg) use ($callback) {
            $callback(new Message($msg));
        };
    }

    public function listen(): void
    {
        call_user_func_array($this->callback, array(base64_decode($this->data->message->data )) );
    }

    public function forever(): void
    {
        $this->listen();
    }

    protected $data;

    public function __construct($params)
    {
        $this->data = json_decode($params);
    }

}
